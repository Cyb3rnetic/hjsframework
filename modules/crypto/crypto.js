/**
 * Title: crypto.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Node and OpenSSL Crypto
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module config
var modconf = {
    "TITLE": "Crypto",
    "DESCRIPTION": " \tThis crypto module supplies access to algorithms supported by the version of OpenSSL on the platform."
}

// Module params
var params = {
    "STRING": "",
    "TYPE": "sha1"
}

var types = [ 'sha', 'sha1', 'sha224', 'sha256', 'sha384', 'sha512', 'md4', 'md5', 'ripemd160', 'dss1', 'whirlpool' ];

// Deps
var crypto = require('crypto');
var hashsum = undefined;

module.exports =
{
    init: function() {
        report.emit('screen', ink.cotag + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function() {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        response += 'Supported Types'.norm+'\n';
        response += '-----------------'.hline+'\n';
        for (var type in types) {
            response += types[type].pvalue+'\n';
        }
        report.emit('screen', response);
    },
    set: function(key, value) {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.cotag +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	{
        report.emit('screen', ink.cotag +modconf['TITLE']+' => '.highlight + 'run method => crypto type ' + params['TYPE'] );   // Launch run method session
        try {
            for (var type in types) {
                if (params['TYPE'] == types[type]) {
                    hashsum = crypto.createHash(params['TYPE'].toLowerCase());
                    hashsum.update(params['STRING']);
                    var d = hashsum.digest('hex');
                    report.emit('screen', d + '\n');
                }
            }
        } catch (e) {
            report.emit('screen', '[Error] '.error + 'Please check the type and try again'.highlight );
        }
        
        prompt.emit('prompt');
    },
    done: function() {
        report.emit('screen', ink.cotag +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};
