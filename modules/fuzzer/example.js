/**
 * Title: example.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Example module
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Example Module",
    "DESCRIPTION": "\tExample module that can be used as a template when starting new mods. "
}

var params = {
	"HOST": "127.0.0.1",
	"PORT": 23,
	"TYPE": "normal"
}


// Module Exports
// All modules require the following functions
// init, lparam, set, run, done, describe
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.cotag + modconf['TITLE']+' => '.highlight + 'initialized methods' ); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.frtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        report.emit('screen', ink.frtag  +modconf['TITLE']+' => '.highlight + 'run method => example module type ' + params['TYPE'] );   // Launch run method session
        // Do your bruteforce, fuzz, verifications, exploit here etc..
        //report.emit('log', thedata); // log to file only without printing to screen
        // emitting a 'screen' call will print screen and log if set true in config.json
        prompt.emit('prompt');
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.frtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};