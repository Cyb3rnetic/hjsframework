/**
 * Title: backdoors.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Search for backdoors on a host website address
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// TODO : Review all backdoors again and add string fingerprints, also add a url crawler

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Remote Backdoor Sniffer",
    "DESCRIPTION": "Sniff out hidden backdoors on a remote website."
};

var params = {
	"HOST": "www.example.com",
    "PORT": 80
};

/**
 * @type {exports|module.exports}
 * Requirements
 */
var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

var http = require('http');


var statusCode = "none";
var url = [];

var directories = ['/', '/public/', '/cgi-bin/', '/sys/', '/root/', '/wp-content/uploads/', '/utilerias/',
    '/wp-content/themes/headway-166/library/resources/timthumb/', '/wp-content/themes/headway-2013/library/resources/timthumb/',
    '/PAGE_GRAPHICS/', '/images/', '/wp-content/themes/OptimizePress/', '/flowplayer/', '/wp-admin/',
    '/wp-content/plugins/', '/wp-content/themes/', '/wp-content/images/', '/includes/', '/wp-includes/', '/template/', '/templates/', '/themes/', '/skins/', '/tmpl/'];

// If you notice any of these that should be .asp please try to contact me. Thanks!
var pages = ['sistems.php', 'log.php', "3.php", "x.php", "X.php", "shell.php", "cookie.txt", "pass.txt", "timthumb.php", "timthumbs.php", "thumbnail.php", "shells.txt", "Commands.php",
    "uploader.php", "version.txt", "phpinfo.php", "test.php", "eval.php", "evil.php", "x2300.php", "casus15.php", "cgitelnet.php", "CmdAsp.asp",
    "dingen.php", "entrika.php", "529.php", "accept_language.php", "Ajax_PHP_Command_Shell.php", "AK-74.php", "AK-74.asp", "Antichat_Shell.php", "antichat.php",
    "aspydrv.php", "ayyildiz.php", "azrailphp.php", "b374k.php", "backupsql.php", "c0derz_shell.php", "c0derzshell.php", "c99", "locus7.php", "locus.php", "madnet.php",
    "madshell.php", "casus.php", "cmdasp.asp", "cpanel.php", "crystalshell.php", "cw.php", "cybershell.php", "dC3.php", "diveshell.php", "dive.php", "dtool.php", "erne.php",
    "fatal.php", "findsock.php", "ftpsearch.php", "g00nshell.php", "gamma.php", "gfs.php", "go-shell.php", "h4ntu.php", "ironshell.php", "kadot.php", "ka_ushell.php", "kral.php", "klasvayv.php",
    "lolipop.php", "Macker.php", "megabor.php", "matamu.php", "lostdc.php", "myshell.php", "mysql_tool.php", "mysql_web.php", "NCC-Shell.php", "nshell", "php-backdoor.php", "PHANTASMA.php",
    "predator.php", "pws.php", "qsd-php", "reader.asp", "ru24.php", "safe0ver.php","rootshell.php", "RemExp", "simattacker.php", "simshell.php", "simple-backdoor.php", "sosyete.asp",
    "small.php", "stres.php", "tryag.php", "toolaspshell.asp", "stnc.asp", "sincap.asp", "winx.asp", "Upload.php", "zaco.asp", "zehir.asp", "zyklon.asp",
    "a.php", "bd.php", "thumbnail.php", "timthumb.php", "timthumbs.php", "config.php", "router.php", "admin.php", "config.php", "controller.php", "cnc.php", "upload.php", "setup.php", "mysql.php", "phpinfo.php", "database.php", "config.inc.php", "connector.php", "example.php", "sql.php", "auth.php", "backup.php", "mysqli.php", "php.php", "json.php",
    "file_manager.php", "sendmail.php", "cron.php", "password.php", "setting.ini.php", "server.php", "database.mysqli.php", "edituser.php", "admin_header.php",
    'Server.php', 'xmlrpcs.php', 'uploadfile.php', 'functions.inc.php'];

var currentUrl = 0;
var current = 0; // added

var totalLoop = 1;
var totalUrl = 0;
var idToFetch = 0;

var found301 = 0;
var found404 = 0;
var found200 = 0;

// Module Exports
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.srtag  + modconf['TITLE']+' => '.highlight + 'initialized methods' ); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        if (params.PORT == 80) {
            //http = require('http');
        } else if(params.PORT == 443) {
            http = require('https');
        }

        report.emit('screen', '['.norm + 'S'.info + '] '.norm + modconf['TITLE'] + ' => '.highlight + 'running => backdoor sniffer ');   // Launch run method session

        reset();

        for (var dir in directories) {
            for (var page in pages) {
                url.push(directories[dir] + pages[page]);
                totalUrl++;
            }
        }

        //report.emit('screen', url.toString());
        //for( var i = 0; i < totalLoop; i++ ) {
        getNextUrl();
        //}
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods');	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};

function getNextUrl(){
    idToFetch = currentUrl++;

    if(idToFetch >= totalUrl){
        report.emit('screen', "\nScan complete!".hline+"\nBackdoors located".info+" => ".highlight+found200+"".red+"\nPossible".info+" => ".highlight+found301+"".error+"\nFailed".info+" => ".highlight+found404+"".red+"\nSee "+CONFIG_DATA.CONFIGS.CORE.FILELOG+" for found file contents"+"\n\n");
        prompt.emit('prompt');
        return;
    };

    var options = {
        host: params.HOST,
        port: params.PORT,
        path: url[idToFetch],
        agent: false,
        pageId: idToFetch
    };

    http.get(options, function(res) {
        var pageData = "";

        res.resume();

        res.on('data', function(chunk) {
            // need to allow user to view at end of scan with option
            // or read MORE by key as processing and implement file logging
            if(res.statusCode == 200) {
                chunk += chunk;
                report.emit('log', chunk + "\n");
            }
        });

        res.on('end', function() {
            if(res.statusCode == 200){
                found200++;
                report.emit('screen', ink.srtag  + options.pageId+ ' ' +options.path + ' Response: 200');
            }
            if(res.statusCode == 301){ found301++; report.emit('screen', '['.norm+'S'.red+'] '.norm + options.pageId+ ' ' +options.path + ' Got '+res.statusCode); }
            if(res.statusCode == 404){ found404++; report.emit('screen', '['.norm+'S'.red+'] '.norm + options.pageId+ ' ' +options.path + ' Got '+res.statusCode); }

            getNextUrl(); //call the next url to fetch
        });

    }).on('error', function(e) {
        report.emit('screen', "Error: " + options.host + "\n" + e.message);
        getNextUrl();
    });
}

function reset() {
    currentUrl = 0;
    current = 0; // added

    totalLoop = 1;
    totalUrl = 0;
    idToFetch = 0;

    found301 = 0;
    found404 = 0;
    found200 = 0;
}
