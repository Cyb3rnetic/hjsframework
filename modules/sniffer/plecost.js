/**
 * Title: plecost.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Plecost implementation into framework
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;
var ink = require('../../lib/colors');

var sys = require('util');
var exec = require('child_process').exec;

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Plecost WP Pluggin Scanner",
    "DESCRIPTION": "\tWordpress finger printer tool (with threads support) 0.2.2-9-beta. Install to ./vendors/"
};

var params = {
	"HOST": "www.example.com",
	"THREADS": 2,
	"PLUGINS": 20,         // There are more than 7000 plugins in the supplied wp database - needs updated though
    "PLUGFILE": "./vendors/plecost-0.2.2-9-beta/wp_plugin_list_2013_feb.txt",
    "CVEONLY": true
};


// Module Exports
// All modules require the following functions
// init, lparam, set, run, done, describe
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.srtag  + modconf['TITLE']+' => '.highlight + 'initialized methods'  ); // Announce initialized module methods
    },
    list: function()
    {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'run method => Plecost WP Plugin Scanner');
        report.emit('screen', ink.srtag  +'Please be patient. Depending on your thread and plugin settings this scan could take awhile to finish.\n\n');

        var cmd = "python ./vendors/plecost-0.2.2-9-beta/plecost-0.2.2-9-beta.py -t "+params.THREADS+" -n "+params.PLUGINS;
        if (params.CVEONLY)
            cmd += " -c";
        cmd += " -i " + params.PLUGFILE + " " + params.HOST;
        //console.log(cmd);process.exit();
        exec(cmd, puts);// run linux bash command

    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};

function puts(error, stdout, stderr) { report.emit('screen', "\n"+stdout+stderr+error); prompt.emit('prompt'); }
