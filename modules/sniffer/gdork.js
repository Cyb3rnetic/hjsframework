/**
 * Title: gdork.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Google dorking module
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */


// TODO
// Add basic and advanced dork querie option params

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;
var ink = require('../../lib/colors');

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Google Dork",
    "DESCRIPTION": " \tPerform Google Dorks manually from inside the freamework and export to JSON/CSV"
}

var google = "https://www.google.com/search?q=";

var params = {
	"DORK": "inurl",
	"KEYWORD": "admin",
	"PAGES": "2"
}

// Module Exports
// All modules require the following functions
// init, lparam, set, run, done, describe
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.srtag + modconf['TITLE']+' => '.highlight + 'initialized methods' ); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
				response += 'Supported Dorks\n';
				response += '-----------------'.hline+'\n';
				response += 'allinurl\n'.pvalue;
				response += 'allintitle\n'.pvalue;
				response += 'inurl\n'.pvalue;
				response += 'intitle\n'.pvalue;
				response += 'ext\n'.pvalue;
				response += 'filetype\n'.pvalue;
				response += 'related\n'.pvalue;
				response += 'cache\n'.pvalue;
				response += 'syntax\n'.pvalue;
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value  );	// Set param value
    },
    run: function()	// Execute module run method
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'run method => example module type ' + params['TYPE'] );   // Launch run method session
				var allpages = [];
				var count = 1;
				while (count <= params['PAGES']) {
					allpages.push(google+params['DORK']+':'+params['KEYWORD']+"&start"+(count*10));
					count++;
				}
			
			        var claw_config = {
			                pages : allpages,
			                selector : 'h3',
			                fields : {
			                        "text" : "$(sel).find('a').text()",
			                        "href" : "$(sel).find('a').attr('href')"
			                },
			                delay : 3
			        };
			        //claw.init(claw_config);
				claw(claw_config);
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};


// the claw
// TODOS
//  - accept a callback function?
//  - add http user-agent override
	
// libararies
var request = require('request'),
	path = require('path'),
	cheerio = require('cheerio'),
	fs = require('fs'),
	__ = require('underscore'),
	jsonfile = require('jsonfile'),
	json2csv = require('json2csv');

var allResults = []; 	
var i = 0;	
var outputFolderDefault = 'output';

var settings = {};

function claw(config) {

	var configObj;
	
	if (__.isString(config)) {
		if (config.search('.json') < 0) {
			config += '.json';
		}		
		var config_path;
		if (config.search('/') == 1 || config.search('.') == 1) config_path = config;
		else config_path = path.join(process.cwd(), config);
		
		if (!fs.existsSync(config_path)) {
		  	//console.log(config + " not found!");
			report.emit('screen', ink.srtag  +' => '.highlight + config + ' not found' ); 
		  	process.kill();
		}
		
		configObj = require(config_path);
	}	
	else configObj = config;
		
	var pageToken = configObj.pages;
	
	if (__.isString(pageToken)) {
		if (pageToken.search('.json') > -1) {
			settings.pages = importPageArr(pageToken);
		} else {
			settings.pages = [pageToken];		
		}
	} else {
		settings.pages = pageToken;		
	}
	
	settings.delay 		= configObj.delay;
	settings.fields 	= configObj.fields;		
	settings.delayMS 	= configObj.delay * 1000;
	
	if (configObj.selector) settings.selector = configObj.selector;
	else settings.selector = "body";
	
	if (configObj.outputFolder) settings.outputFolder = config.outputFolder;
	else if (__.isString(config)) settings.outputFolder = path.basename(config, '.json');
	else settings.outputFolder = outputFolderDefault;
			
	// check if output folder exists						
	if (!fs.existsSync(settings.outputFolder)) {			
	  	fs.mkdirSync(settings.outputFolder);
	}
			
	// init			
	//console.log("Starting scraper...");
	report.emit('screen', ink.srtag  + ' => '.highlight + 'starting scraper' ); 
	scraperLoop(settings.delayMS);		
			
	// functions
	
	function scraperLoop() {
		scrapePage(i);
		i++;
		if (i < settings.pages.length) {
			// console.log("Waiting " + settings.delay + " seconds...");
			setTimeout(scraperLoop, settings.delayMS);
		} else {
			setTimeout(endLoop, settings.delayMS);
		}
	}
	
	function endLoop() {
		//console.log("\n" + i + " pages scraped!");
		report.emit('screen', ink.srtag  +' => '.highlight + i + ' pages scraped!'); 
		allResults = [];
		i = 0;
		prompt.emit('prompt');
	}
	
	function scrapePage(index) {
	
		var page = settings.pages[index];
	
		//console.log("\nScraping " + page);
		report.emit('screen', ink.srtag  +' => '.highlight + 'Scraping ' + page ); 
	
		request(page, function(err, resp, body){
		
			$ = cheerio.load(body);
			var results = [];
			
			$(settings.selector).each(function(i, sel){
			
				var output = {};
				
				for (var prop in settings.fields) {					
					var fieldJS = "output['" + prop + "'] = " + settings.fields[prop];
					eval(fieldJS);
					//report.emit('screen', ink.srtag  +' => '.highlight + output['text'] + ' => ' + output['href']  +'\n');					      
				}	
				//var re = /\/url?=/gi;
				//var str = output['href'];
				//var newstr = str.replace(re, '');					
				//eport.emit('screen', ink.srtag  +' => '.highlight + output['text'] + ' => ' + output['href'].replace('/url?q=', ''));
				report.emit('screen', ink.srtag  +' => '.highlight + output['text'] + ' => ' + output['href']);
				results.push(output);
				//report.emit('screen', ink.srtag  +' => '.highlight + output  +'\n'); 
			});
				//console.log(results);
			//report.emit('screen', ink.srtag  +' => '.highlight + results  +'\n'); 
			allResults[index] = results;
			
			var outfile;
			
			if (settings.pages.length === 1) outfile = "output";
			else outfile = index;
			
			exportJSON(results, outfile);
			exportCSV(results, outfile);
		
					
		});		
		
	}

	function exportJSON(results, filename) {
	
		if (filename === undefined) filename = "output";
		var export_file = path.join(settings.outputFolder, filename + '.json');
		
		//console.log("Exporting " + export_file);
		report.emit('screen', ink.srtag  +' => '.highlight + 'exporting to => ' + export_file ); 
		jsonfile.writeFileSync(export_file, results);
				
	}

	function exportCSV(results, filename) {
	
		if (filename === undefined) filename = "output";
		var export_file = path.join(settings.outputFolder, filename + '.csv');
		
		var fieldArr = [];
		
		for (var prop in settings.fields) {
		      fieldArr.push(prop);
		}			
		
		json2csv({data: results, fields: fieldArr}, function(err, csv) {
		  if (err) console.log(err);
		  fs.writeFile(export_file, csv, function(err) {
			if (err) throw err;
		    //console.log("Exporting " + export_file);
		    report.emit('screen', ink.srtag  +' => '.highlight + 'exporting to => ' + export_file  ); 
		  });
		});				
		
	}		

}

function importPageArr(page_token) {

	var json_path;
	
	if (page_token.search('/') == 1 || config.search('.') == 1) json_path = config;
	else json_path = path.join(process.cwd(), page_token);

	var inputArr = require(json_path);
	var outputArr = [];
	
	inputArr.forEach(function(page) {
		if (__.isString(page)) outputArr.push(page);
		else outputArr.push(page.href);
	});
	
	return outputArr;

}

exports.init = claw;

// command line version
var arg = process.argv[2];
if (__.isString(arg)) {
	exports.init(arg);
}
