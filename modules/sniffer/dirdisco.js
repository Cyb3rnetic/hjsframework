/**
 * Title: dirdisco.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Discover directories on a web address
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */

var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

// Module parameters
var modconf = {
    "TITLE": "Directory Discovery",
    "DESCRIPTION": "Sniff out directories on a web server."
};

var params = {
	"HOST": "www.example.com",
    "PORT": 80
};

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

var http = require('http');

var statusCode = "none";
var url = [];

// Some preloaded examples
var directories = ['/a/', '/b/', '/c/', '/d/', '/e/', '/f/', '/g/', '/h/', '/i/', '/j/', '/k/', '/l/', '/m/', '/n/', '/o/', '/p/', '/q/', '/r/', '/s/', '/t/', '/u/', '/v/', '/w/', '/x/', '/y/', '/z/',
    '/install/', '/sql/', '/installation/', '/backups/', '/backup/', '/back-ups/', '/hidden/', '/logs/','/sql-databases/', '/application/_installation/sql_statements/',
    '/docs/', '/notes/', '/documents/', '/private/', '/secure/', '/old/', '/secret/', '/config/', '/configuration/', '/configurations/',
    '/data/', '/database/', '/dox/', '/keepout/', '/donotdelete/', '/themes/', '/exec/', '/level/', '/classes/', '/includes/', '/system/', '/root/', '/symlink/',
    '/sym/', '/symlnk/', '/user/', '/js/', '/cgi-bin/', '/plugins/', '/README/', '/templates/', '/tmpl/', '/modules/', '/error_log/', '/views/', '/lib/', '/db/', '/faqweb/', '/system32/', '/skins/', '/_vti_cnf/', '/cache/', '/CVS/', '/main/', '/update/', '/extensions/', '/jscripts/', '/categoryblog/'];

var currentUrl = 0;
var current = 0; // added

var totalLoop = 1;
var totalUrl = 0;
var idToFetch = 0;

var found301 = 0;
var found404 = 0;
var found200 = 0;

// Module Exports
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.srtag  + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function()
    {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        if (params.PORT == 80) {
            //http = require('http');
        } else if(params.PORT == 443) {
            http = require('https');
        }

        report.emit('screen', '['.norm + 'S'.info + '] '.norm + modconf['TITLE'] + ' => '.highlight + 'running => directory sniffer ');   // Launch run method session

        reset();

        for (var dir in directories) {
            url.push(directories[dir]);
            totalUrl++;
        }

        getNextUrl();
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods');	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};

function getNextUrl(){
    idToFetch = currentUrl++;

    if(idToFetch >= totalUrl){
        report.emit('screen', "\nScan complete!".debug+"\nDirectories located".info+" => ".highlight+found200+"".red+"\nPossible".info+" => ".highlight+found301+"".error+"\nFailed".info+" => ".highlight+found404+"".red+"\nSee "+CONFIG_DATA.CONFIGS.CORE.FILELOG+" for found file contents"+"\n\n");
        prompt.emit('prompt');
        return;
    };

    var options = {
        host: params.HOST,
        port: params.PORT,
        path: url[idToFetch],
        agent: false,
        pageId: idToFetch
    };

    http.get(options, function(res) {
        var pageData = "";

        res.resume();

        res.on('data', function(chunk) {
            // need to allow user to view at end of scan with option
            // or read MORE by key as processing and implement file logging
            if(res.statusCode == 200) {
                chunk += chunk;
                report.emit('log', chunk + "\n");
            }
        });

        res.on('end', function() {
            if(res.statusCode == 200){
                found200++;
                report.emit('screen', ink.srtag  + options.pageId+ ' ' +options.path + ' Response: 200');
            }
            if(res.statusCode == 301){ found301++; report.emit('screen', '['.norm+'S'.error+'] '.norm + options.pageId+ ' ' +options.path + ' Got '+res.statusCode); }
            if(res.statusCode == 404){ found404++; report.emit('screen', '['.norm+'S'.error+'] '.norm + options.pageId+ ' ' +options.path + ' Got '+res.statusCode); }

            getNextUrl(); //call the next url to fetch
        });

    }).on('error', function(e) {
        report.emit('screen', "Error: " + options.host + "\n" + e.message);
        getNextUrl();
    });
}


function reset() {
    currentUrl = 0;
    current = 0; // added

    totalLoop = 1;
    totalUrl = 0;
    idToFetch = 0;

    found301 = 0;
    found404 = 0;
    found200 = 0;
}
