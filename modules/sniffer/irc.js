 /**
 * Title: irc.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: IRC sniffer. Gather intel from IRC Servers
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// TODO:
// Scan multiple servers, one server at a time
// Increase performance between channel joins and data sniffing
// Add proxy support

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;
var ink = require('../../lib/colors');
var irc = require("irc");
var fs = require('fs');
var util = require("util");

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "IRC Sniffer",
    "DESCRIPTION": "\tThe IRC Sniffer will connect to a list of IRC servers and search for keywords in channel names, topics, nicknames etc.."
};

var params = {
	"SERVER": "irc.freenode.net",
	"KEYWORD": "Gods",
	"LOGGING": 0,
	"LOG": "log/irclog"
};

var config = {
    userName: 'hjs-irc',
    realName: 'hjs IRC client',
    port: 6667,
    localAddress: null,
    debug: false,
    showErrors: false,
    autoRejoin: false,
    autoConnect: false,
    channels: ['#hjs'],
    secure: false,
    selfSigned: false,
    certExpired: false,
    floodProtection: false,
    floodProtectionDelay: 1000,
    sasl: false,
    stripColors: false,
    channelPrefixes: "&#",
    messageSplit: 512,
    encoding: ''
};


//var servers = ['irc.opcodedevteam.tk', 'irc.geekshed.net'];

module.exports =
{
    init: function()		
    {
        report.emit('screen', ink.srtag + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'launching data collection process @ log level ' + params['LOGGING'] );   // Launch run method session
				
				//for (var server in servers) {
					//report.emit('screen', servers[server]);
					doScan(params['SERVER']);
				//};
    },
    done: function()	
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods and disconnected from servers' );	// Done using module
    		bot.disonnect();
    },
    describe: function()	
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};

function writelog(filename, content) {
	fs.appendFile(filename, content, function (err) {
	  if (err) return;
	});
}

function doScan (server) {
		var bot = new irc.Client(server, 'nodejs', config);
	
		bot.addListener("raw", function(message) {
			//report.emit('screen', util.inspect(message, false, null));
			if (message.rawCommand == '376') {
			    bot.list();
			}
		});
		
		bot.addListener("channellist_item", function(channellist_info) {
			//report.emit('screen',channellist_info.name);
			bot.join(channellist_info.name);
		});
		
		bot.addListener("names", function(channel, nicks) {
			if (channel.toString().indexOf(params['KEYWORD']) != -1) {
				report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + channel + ' TITLE => ' + params['KEYWORD']);
				if (params['LOGGING'] == 1) {
					writelog (params['LOG'], modconf['TITLE']+' => '.highlight + channel + ' TITLE => ' + params['KEYWORD'] + '\n');
				}
			}

			if (util.inspect(nicks, false, null).indexOf(params['KEYWORD']) != -1 ) {
				report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + channel + ' NICKS => ' + params['KEYWORD']);
				if (params['LOGGING'] == 1) {
					writelog (params['LOG'],  modconf['TITLE']+' => '.highlight + channel + ' NICKS => ' + params['KEYWORD'] + '\n');
				}
			}
			
		});
		
		bot.addListener("topic", function(channel, topic, nick, message) {
			if (channel.indexOf(params['KEYWORD']) != -1) {
				report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + channel + ' => ' + params['KEYWORD']);
				if (params['LOGGING'] == 1) {
					writelog (params['LOG'], modconf['TITLE']+' => '.highlight + channel + ' => ' + params['KEYWORD'] + '\n');
				}
			}
			if (topic.indexOf(params['KEYWORD']) != -1) {
				report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + channel + ' => TOPIC => ' + params['KEYWORD']);
				if (params['LOGGING'] == 1) {
					writelog (params['LOG'], modconf['TITLE']+' => '.highlight + channel + ' => TOPIC => ' + params['KEYWORD'] + '\n');
				}
			}
			if (nick.indexOf(params['KEYWORD']) != -1) {
				report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + channel + ' => TOPIC SET BY => ' + params['KEYWORD']);
				if (params['LOGGING'] == 1) {
					writelog (params['LOG'], modconf['TITLE']+' => '.highlight + channel + ' => TOPIC SET BY => ' + params['KEYWORD'] + '\n');
				}
			}	
		});

		bot.connect();		
			
		prompt.emit('prompt');
}

