/**
 * Title: xst.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Check website for XST vulnerabilities
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// HTTP module
var http = require('http');

// Module configuration
var modconf = {
    "TITLE": "XST",
    "DESCRIPTION": "   \tModule used to detect Cross-Site Tracing (XST) vulnerabilities."
}

// Module Params
var params = {
	"HOST": "www.example.com",
	"PORT": 80,
    "PATH": "/",
    "HEADER": "XSTCheck",
    "METHOD": "TRACE"
}

// Request options
var options = {
    host: params.HOST,
    port: params.PORT,
    path: params.PATH,
    headers: {
        'CustomHeaderCheck': params.HEADER
    },
    method: params.METHOD
};

// Module Exports
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.srtag  + modconf['TITLE']+' => '.yellow + 'initialized methods'); // Announce initialized module methods
    },
    list: function()
    {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].yellow+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.yellow + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        if (params.PORT == 80) {
            //http = require('http');
        } else if(params.PORT == 443) {
            http = require('https');
        }

        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'run method => XST check');   // Launch run method session
        try {
            var req = http.request(options, function(res) {
                if (res.statusCode === 200) {
                    var headers = res.headers;
                    // If our header exists in the return data
                    if (headers.CustomHeaderCheck) {
                        report.emit('screen', 'VULNERABLE: Site responded with custom header. It is subject to XST attacks\n\n'.error);
                        prompt.emit('prompt');
                    } else {
                        report.emit('screen', 'SAFE: This site does not appear to be subject to XST\n\n'.info);
                        prompt.emit('prompt');
                    }
                } else {
                    report.emit('screen', 'SAFE: This site does not appear to be subject to XST\n\n'.info);
                    prompt.emit('prompt');
                }

                res.on('data', function(chunk) {

                });
            });

            req.on('error', function(e) {
                report.emit('screen', 'There was a problem with the request, which might mean TRACE is not supported.\n\n'.highlight);
                report.emit('screen', 'To be safe, try the request again on port 443 for HTTPS.\n\n'.highlight);
                report.emit('screen', 'Error:'.highlight + ' \"' + e.message + '\"\n\n');
                prompt.emit('prompt');
            });

            req.end();
        } catch (err) {
            report.emit('screen', err+"\n\n");
        }

    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.srtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};
