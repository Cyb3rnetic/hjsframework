/**
 * Title: honeypot.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: A basic honeypot
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// NOTES
// ---------
// Telnet server will crash upon nmap scan. Error in net.js - could this be a vulnerability or could it be used for badness?

/**
 * @type {exports|module.exports}
 * Requirements
 */
var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;
var ink = require('../../lib/colors');

// HTTP Honeypot requirements
var server =  undefined;
var http = require('http');
var qs = require('querystring');

// Telnet Honeypot requirements
var net = require('net');
var sockets = [];

// Logging requirements
var fs = require('fs');
var util = require('util');
//var sys = require('sys');
var exec = require('child_process').exec; 
	
// Use for generating hashes
var crypto = require('crypto');
var walk   = require('walk');
var files  = [];
var jsonfile = require('jsonfile');
var file = './file.md5';

// Debugging requirements util.inspect(var, false, null)
var util = require("util");


// Module configurations
var modconf = {
    "TITLE": "Honeypot",
    "DESCRIPTION": "A basic honeypot with HTTP, TELNET, and FILE monitoring. See: ./lib/html/honeypot"
};

// Module params
var params = {
	"HTTP": 1,
	"TELNET": 1,
	"WATCH": 1,
	"HTTP_PORT": 3000,
  "STATUS": 200,
  "INDEX": "./lib/html/honeypot/index.html",
	"CONTENT_TYPE": "text/html",
	"TELNET_PORT": 8888,
	"WATCH_DIR": "/home/user/irclogs",
	"WATCH_INTERVAL": 50000,
	"MD5LOG": "./log/file.md5",
	"SCREENLOG": 1,
	"LOGGING": 0,
	"LOG": "./log/honeypot"
};

// Watch interval
var interval;

// Module Exports
module.exports =
{
    init: function() {
        report.emit('screen', ink.setag  + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	{
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value) {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value);	// Set param value
    },
    run: function()	{
    	
		if (params['WATCH'] == 1) {
		// Start file monitor
			generateHashes ();
		}
			
		if (params['HTTP'] == 1) {
    		// Create our HTTP honeypot
	        server = http.createServer(onRequest).listen(parseInt(params['HTTP_PORT']));
	        report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'STARTED' +' => '.highlight + 'HTTP' + ' => '.highlight + 'PORT ' + params['HTTP_PORT']);   
        }
       	
       	if (params['TELNET'] == 1) {
	        // Create a new telnet honeypot and provide a callback for when a connection occurs
			var telnet = net.createServer(newSocket);
			telnet.listen(params['TELNET_PORT']);
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'STARTED' +' => '.highlight + 'TELNET' + ' => '.highlight + 'PORT ' + params['TELNET_PORT']);   
		}

		prompt.emit('prompt');
    },
    done: function() {
        report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods');	// Done using module
        server.close(); //close the server
        server = "";
        telnet.close();
        socket.end();
        telnet = "";
        clearInterval(interval);
        interval = "";
    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

// On web request
function onRequest(request, response) {
		// Visual logging to the screen if enabled
		if (params['SCREENLOG'] == 1) {
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'HTTP' + ' => '.highlight + 'SRC HOST ' + '=> '.highlight + request.headers.host );
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'HTTP' + ' => '.highlight + 'REFERER ' + '=> '.highlight + request.headers.referer );
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'HTTP' + ' => '.highlight + 'URL ' + '=> '.highlight + request.url );
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'HTTP' + ' => '.highlight + 'METHOD ' + '=> '.highlight + request.method );
		}
		
		// Write the logs if logging is enabled in params
    if (params['LOGGING'] == 1) {
			writelog (params['LOG'], modconf['TITLE']+' => ' + 'HTTP' + ' => ' + 'SRC HOST ' + '=> ' + request.headers.host + "\n");
			writelog (params['LOG'], modconf['TITLE']+' => ' + 'HTTP' + ' => ' + 'REFERER ' + '=> ' + request.headers.referer + "\n");
			writelog (params['LOG'], modconf['TITLE']+' => ' + 'HTTP' + ' => ' + 'URL ' + '=> ' + request.url + "\n");
			writelog (params['LOG'], modconf['TITLE']+' => ' + 'HTTP' + ' => ' + 'METHOD ' + '=> ' + request.method + "\n");
    }
    
    // Handle POST requests
    if (request.method == 'POST') {
        var body = '';
        request.on('data', function (data) {
            body += data;
            // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
            if (body.length > 1e6) { 
                // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
                request.connection.destroy();
            }
        });
        request.on('end', function () {
            var POST = qs.parse(body);
            for (var p in POST) {
            	report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'HTTP' + ' => '.highlight + 'POST ' + p + ' => '.highlight + POST[p]);
            	if (params['LOGGING'] == 1) {
								writelog (params['LOG'], modconf['TITLE']+' => ' + 'HTTP' + ' => ' + 'POST ' + p + ' => ' + POST[p] + "\n");
							}
            }
        });
    }
		
		// Server content and close the socket
    response.writeHead(parseInt(params['STATUS']), {"Content-Type": params['CONTENT_TYPE']});
    response.write(fs.readFileSync(params['INDEX'])); 
    response.end();
    request.connection.end(); //close the socket
    request.connection.destroy; //close it really
}


// Cleans the input of carriage return, newline
function cleanInput(data) {
	return data.toString().replace(/(\r\n|\n|\r)/gm,"");
}
 
// Method executed when data is received from a socket
function receiveData(socket, data) {
	try{
		var cleanData = cleanInput(data);
		
		if(cleanData === "pwd") {
			socket.write('/\n');
			socket.write('[/]$ ');
		}
		
		if(cleanData === "ls") {
			socket.write('bin    dev   initrd.img      lib32       media  proc  sbin  tmp  vmlinuz\n');
			socket.write('boot   etc   initrd.img.old  lib64       mnt    root  srv   usr  vmlinuz.old\n');
			socket.write('cdrom  home  lib             lost+found  opt    run   sys   var\n');
			socket.write('[/]$ ');
		}
		
		if(cleanData === "help") {
			socket.write('GNU bash, version 4.3.11(1)-release (x86_64-pc-linux-gnu)\n');
			socket.write('These shell commands are defined internally.  Type `help` to see this list.\n');
			socket.write('Type `help name` to find out more about the function `name`.\n');
			socket.write('Use `info bash` to find out more about the shell in general.\n');
			socket.write('Use `man -k` or `info` to find out more about commands not in this list.\n\n');
			
			socket.write('A star (*) next to a name means that the command is disabled.\n');
			socket.write('exit [n]                                echo [-neE] [arg ...]\n');
			socket.write('help [-dms] [pattern ...]\n\n');
			socket.write('[/]$ ');
		}
		
		if(cleanData.indexOf('ps') != -1) {
		  socket.write('PID TTY          TIME CMD\n');
			socket.write('21071 pts/12   00:00:00 bash\n');
			socket.write('21526 pts/12   00:00:00 ps\n');
			socket.write('critical error :can report a snapshot of the current processes\n');
			socket.write('[/]$ ');
		}
		
		if(cleanData === "cat /etc/passwd") {
			socket.write('Looking for something?\n');
			socket.write('[/]$ ');
		}
		
		if(cleanData.indexOf('cd /root') != -1) {
			socket.write('bash: cd: lawlz: No such file or directory\n');
			socket.write('[/]$ ');
		}
		
		if(cleanData.indexOf('cd') != -1) {
			if(cleanData.indexOf('cd /root') != -1) {
			} else {
				socket.write('bash: cd: '+cleanData+': No such file or directory\n');
				socket.write('[/]$ ');
			}
		}
		
		if(cleanData === "exit") {
			socket.end('Goodbye!\n');
		} 

		if(cleanData === '') {
			socket.write('[/]:~$ ');
		}
		
		// Send incoming data to all other teminals.
		//else {
		//	for(var i = 0; i<sockets.length; i++) {
		//		if (sockets[i] !== socket) {
		//			sockets[i].write(data);
		//		}
		//	}
		//}
		
		if (params['SCREENLOG'] == 1) {
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'TELNET' + ' => '.highlight + 'INCOMING DATA ' + '=> '.highlight + cleanData);
		}
		if (params['LOGGING'] == 1) {
			writelog (params['LOG'], modconf['TITLE']+' => ' + 'TELNET' + ' => ' + 'INCOMING DATA ' + '=> ' + cleanData + "\n");
		}
	} catch (e) {
		report.emit('screen', 'Telnet Error');
	}
}
 
// Method executed when a socket ends
function closeSocket(socket) {
		var i = sockets.indexOf(socket);
		if (i != -1) {
			sockets.splice(i, 1);
		}
}
 
// Callback method executed when a new TCP socket is opened.
function newSocket(socket) {
	try {
		sockets.push(socket);
		
		// User connect notice
		if (params['SCREENLOG'] == 1) {
			report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'TELNET' + ' => '.highlight + 'INCOMING CONNECTION ' + '=> '.highlight );
		}
		
		socket.write('[/]$ ');
		socket.on('data', function(data) {
			receiveData(socket, data);
		});
		socket.on('end', function() {
			closeSocket(socket);
		});
	} catch (e) {
		report.emit('screen', e);
	}
}

function puts(error, stdout, stderr) { util.puts(stdout); }

function generateHashes () {
 	exec("rm "+params['MD5LOG']); // instead of remove, if file exists keep it, has to be based on if dirs have changed though
	// Walker options
	var walker  = walk.walk(params['WATCH_DIR'], { followLinks: false });
	
	var filemd5s = [];
	
	walker.on('file', function(root, stat, next) {
	    // Add this file to the list of files
	    //files.push(root + '/' + stat.name);
	    
	      // the file you want to get the hash    
				var fd = fs.createReadStream(root + '/' + stat.name);
				var hash = crypto.createHash('md5');
				hash.setEncoding('hex');
				
				// read all file and pipe it (write it) to the hash object
				fd.pipe(hash);
				
				fd.on('end', function() {
				    hash.end();
				    if (params['SCREENLOG'] == 1) {
				    	report.emit('screen', ink.setag + modconf['TITLE']+' => '.highlight + 'WATCHING' +' => '.highlight + root + '/' + stat.name); // the desired hash
				    }
				    filemd5s.push(root + '/' + stat.name+'&'+hash.read());
				});
				
	    next();
	});
	
	walker.on('end', function() {
	    writelog(params['MD5LOG'], filemd5s);
	    interval = setInterval(function() {
			  readHashes();
			}, params['WATCH_INTERVAL']);
	});

}

function readHashes () {
	
			var savedfilemd5s;
			
			fs.readFile(params['MD5LOG'], 'utf8', function (err,data) {
			  if (err) {
			    return report.emit('screen', util.inspect(err, false, null));
			  }
			  //report.emit('screen', data);
			  savedfilemd5s = data.split(',');
				
			  for (var tmpfile in savedfilemd5s) {
			  	report.emit('screen',savedfilemd5s[tmpfile]);
			  	// the file you want to get the hash 
			  	var tmp = undefined;  
			  	var tmp = savedfilemd5s[tmpfile].split('&'); 
					var fd = fs.createReadStream(tmp[0]);
					var hash = crypto.createHash('md5');
					hash.setEncoding('hex');
					
					var thefile = undefined; 
					var thefile = tmp[0];
					
					// read all file and pipe it (write it) to the hash object
					fd.pipe(hash);
					
					
					fd.on('end', function() {
					    hash.end();
					    thefile = thefile+'&'+hash.read();
					    var savedfile = savedfilemd5s[tmpfile];
					    if(thefile.replace(/&null/g, "") == savedfile) {
					    	if (params['SCREENLOG'] == 1) {
					  			report.emit('screen', ink.setag + modconf['TITLE']+' => '.highlight + 'WATCHING' +' => '.highlight + 'FILE MATCHES! ' +thefile.replace(/&null/g, "") + ' => '.highlight + savedfile);
					  		}
					  	} else {
					  		if (params['SCREENLOG'] == 1) {
					  			report.emit('screen', ink.setag + modconf['TITLE']+' => '.highlight + 'WARNING' +' => '.highlight + savedfile + ' HAS BEEN MODIFIED! => ' + thefile.replace(/&null/g, ""));
					  		}
					  		if (params['LOGGING'] == 1) {
					  			writelog (params['LOG'], modconf['TITLE']+' => ' + 'WARNING' +' => ' + savedfile + ' HAS BEEN MODIFIED! => ' + thefile.replace(/&null/g, "") + "\n");
					  		}
					  	}
					  	
					});
			  }

		  });
	
}

// Write to our custom log file set in params['LOG']
function writelog(filename, content) {
	fs.appendFile(filename, content, function (err) {
	  if (err) return report.emit('screen', util.inspect(err, false, null));
	});
}