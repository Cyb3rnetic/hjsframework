/**
 * Title: http.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: HTTP Service. Used for hosting browser exploits
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

var http = require('http');
var fs = require('fs');
//var prompt = require('prompt');
var server =  undefined;

// Module parameters
var modconf = {
    "TITLE": "HTTP Server",
    "DESCRIPTION": "  \tBasic HTTP service used for hosting browser exploits. See: ./lib/html/exploits"
}

var params = {
	"PORT": 4444,
    "STATUS": 200,
    "INDEX": "./lib/html/index.html",
	"CONTENT_TYPE": "text/plain"
}

// Module Exports
module.exports =
{
    init: function() {
        report.emit('screen', ink.setag  + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	{
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response)
    },
    set: function(key, value) {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	{
        server = http.createServer(onRequest).listen(parseInt(params['PORT']));
        report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'run method => HTTP => PORT ' + params['PORT'] );   // Launch run method session
        prompt.emit('prompt');
    },
    done: function() {
        report.emit('screen', ink.setag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods');	// Done using module
        server.close(); //close the server
        server = "";
    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

function onRequest(request, response) {
    response.writeHead(parseInt(params['STATUS']), {"Content-Type": params['CONTENT_TYPE']});
    response.write(fs.readFileSync(params['INDEX'])); //fs.readFileSync(modconf['INDEX'])
    response.end();
    request.connection.end(); //close the socket
    request.connection.destroy; //close it really
}
