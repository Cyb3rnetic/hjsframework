/**
 * Title: threats.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Threat feeds and visual map directory listing
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Threats",
    "DESCRIPTION": " \tUnited States cyber threat intelligence directory"
};

var params = {
	
};

var livemaps = {
	"NORSE": "http://map.ipviking.com",
	"HONEYNET": "http://map.honeynet.org/",
	"DIGITAL": "http://www.digitalattackmap.com/",
	"FIREEYE": "https://www.fireeye.com/cyber-map/threat-map.html",
	"KASPERSKY": "http://cyberwar.kaspersky.com/",
	"F-SECURE": "http://worldmap3.f-secure.com/",
	"OPENDNS": "http://labs.opendns.com/global-network/",
	"SICHER": "http://www.sicherheitstacho.eu/"
};


// Module Exports
// All modules require the following functions
// init, lparam, set, run, done, describe
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.intag + modconf['TITLE']+' => '.highlight + 'initialized methods' ); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'run method => listing United States Cyber Threat Intelligence directory' );   
	
    	for (var tsite in livemaps)
    		report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + tsite + ' => '.highlight +  livemaps[tsite] );
        
        prompt.emit('prompt');
	
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};
