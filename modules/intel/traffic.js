/**
 * Title: traffic.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Traffic camera directory listing
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Traffic",
    "DESCRIPTION": " \tUnited States traffic camera directory"
};

var params = {
	"STATE": "ALL",
};

var usatraffic = {
	"Alabama": 	"http://alitsweb2.dot.state.al.us/its/",
	"Alaska": 	"http://www.dot.state.ak.us/iways/roadweather/forms/AreaSelectForm.html",
	"Arizona": 	"http://www.az511.gov/adot/files/cameras/",
	"Arkansas": 	"http://www.fhwa.dot.gov/Trafficinfo/ar.htm",
	"California": 	"http://www.dot.ca.gov/dist6/cctv/",
	"Colorado": 	"http://www.cotrip.org/home.htm",
	"Connecticut": 	"http://www.ct.gov/dot/cwp/view.asp?a=1993&Q=290242&dotNav=|",
	"Delaware": 	"http://www.deldot.gov/information/travel_advisory/",
	"Florida": 	"http://www.fl511.com/Cameras.aspx",
	"Georgia": 	"http://www.georgia-navigator.com/cameras",
	"Hawaii": 	"http://www.fhwa.dot.gov/Trafficinfo/hi.htm",
	"Idaho": 	"http://511.idaho.gov/staticMap.asp?display=cams",
	"Illinois": 	"http://www.webcamlocator.com/traffic/illinois_traffic_webcam_locator.htm",
	"Indiana": 	"http://www.fhwa.dot.gov/Trafficinfo/in.htm",
	"Iowa": 	"http://www.webcamlocator.com/traffic/iowa_traffic_webcam_locator.htm",
	"Kansas": 	"http://511.ksdot.org/KanRoadPublic_VE/Default.aspx",
	"Kentucky": 	"http://www.webcamlocator.com/traffic/kentucky_traffic_webcam_locator.htm",
	"Louisiana": 	"http://hb.511la.org/",
	"Maine": 	"http://www.webcamlocator.com/traffic/maine_traffic_webcam_locator.htm",
	"Maryland": 	"http://www.chart.state.md.us/TravInfo/trafficcams.php",
	"Massachusetts": "http://www1.eot.state.ma.us/",
	"Michigan": 	"http://www.webcamlocator.com/traffic/michigan_traffic_webcam_locator.htm",
	"Minnesota": 	"http://www.dot.state.mn.us/tmc/trafficinfo/metrocams/mapindex.html",
	"Mississippi": 	"http://www.mdottraffic.com/default.aspx?showMain=true",
	"Missouri": 	"http://www.webcamlocator.com/traffic/missouri_traffic_webcam_locator.htm",
	"Montana": 	"http://www.mdt.mt.gov/travinfo/weather/rwis.shtml",
	"Nebraska": 	"http://www.webcamlocator.com/traffic/nebraska_traffic_webcam_locator.htm",
	"Nevada": 	"http://www.nvfast.org/trafficcameras.html",
	"New Hampshire": "http://67.62.24.187/reportservice/(S(p4omokq3rplgpufohciafkv3))/public.aspx",
	"New Jersey": 	"http://www.webcamlocator.com/traffic/newjersey_traffic_webcam_locator.htm",
	"New Mexico": 	"http://nmroads.com/",
	"New York": 	"http://www.webcamlocator.com/traffic/newyork_traffic_webcam_locator.htm",
	"North Carolina": "https://apps.dot.state.nc.us/TIMS/RegionSummary.aspx?re=",
	"North Dakota": "http://www.dot.nd.gov/travel-info/",
	"Ohio": 	"http://www.webcamlocator.com/traffic/ohio_traffic_webcam_locator.htm",
	"Oklahoma": 	"http://www.oktraffic.org/index.php",
	"Oregon": 	"http://www.webcamlocator.com/traffic/oregon_traffic_webcam_locator.htm",
	"Pennsylvania": "http://www.webcamlocator.com/traffic/pennsylvania_traffic_webcam_locator.htm",
	"Rhode Island": "http://www.tmc.state.ri.us/",
	"South Carolina": "http://www.511sc.org",
	"South Dakota": "http://www.safetravelusa.com/sd/cameras/",
	"Tennessee": 	"http://www.webcamlocator.com/traffic/tennessee_traffic_webcam_locator.htm",
	"Texas": 	"http://www.webcamlocator.com/traffic/texas_traffic_webcam_locator.htm",
	"Utah": 	"http://commuterlink.utah.gov/",
	"Vermont": 	"http://www.511vt.com/",
	"Virginia": 	"http://511virginia.org/Cameras.aspx?r=1",
	"Washington": 	"http://www.webcamlocator.com/traffic/washington_traffic_webcam_locator.htm",
	"Washington DC":"http://www.webcamlocator.com/traffic/district_traffic_webcam_locator.htm",
	"West Virginia":"http://www.webcamlocator.com/traffic/westvirginia_traffic_webcam_locator.htm",
	"Wisconsin": 	"http://www.webcamlocator.com/traffic/wisconsin_traffic_webcam_locator.htm",
	"Wyoming": 	"http://www.webcamlocator.com/traffic/wyoming_traffic_webcam_locator.htm"
};


// Module Exports
// All modules require the following functions
// init, lparam, set, run, done, describe
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.intag + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'run method => listing United States Traffic Camera directory');   
	
		if (params['STATE'] == 'ALL' || params['STATE'] == 'all' || params['STATE'] == '') {
			for (var state in usatraffic)
				report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + state + ' => '.highlight +  usatraffic[state] );
		}

		prompt.emit('prompt');
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods');	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};
