/**
 * Title: radars.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Radar and weather system directory listing
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */
/**
 * @type {exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../../lib/processor').CONFIG_DATA;

var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module parameters
// All modules require a TITLE and DESCRIPTION
var modconf = {
    "TITLE": "Radars",
    "DESCRIPTION": "  \tUnited States radar, satillite and map directory"
};

var params = {
	
};

var radars = {
	"WDTINC": "http://content.wdtinc.com/clients/wmbf/map.php?MAPID=12818&&CLIENTID=2056",
};


// Module Exports
// All modules require the following functions
// init, lparam, set, run, done, describe
module.exports =
{
    init: function()		// Initialize module
    {
        report.emit('screen', ink.intag + modconf['TITLE']+' => '.highlight + 'initialized methods' ); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	// List module params
    {
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].toString().pvalue+'\n';
        response += '------------------------------------------'.hline+'\n';
        report.emit('screen', response);
    },
    set: function(key, value)	// Set module params
    {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value );	// Set param value
    },
    run: function()	// Execute module run method
    {
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'run method => listing United States Radar, Satillite and Map directory' );   
	
	
        for (var state in radars)
            report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + state + ' => '.highlight +  radars[state] );

        prompt.emit('prompt');
	
    },
    done: function()		// Done using module
    {
        report.emit('screen', ink.intag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function()	// Done using module
    {
        return modconf['DESCRIPTION'];	// Describe itself
    }
};
