/**
 * Title: buffer.js
 * Type: Module
 * Author: Hack.JS Framework
 * Description: Buffer strings
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var prompt = require('../../lib/processor').promptEmitter;
var report = require('../../lib/report').reportEmitter;

var ink = require('../../lib/colors');

// Module parameters
var modconf = {
    "TITLE": "Buffer",
    "DESCRIPTION": "   \tAccess to NodeJS Buffer for performing basic hashing commonly used when handling buffers."
}

var params = {
    "STRING": "",
	"TYPE": "base64"  // types: base64, ascii
}

var types = [ 'base64', 'hex', 'utf8', 'ucs2', 'base64_ascii', 'hex_ascii', 'utf8_ascii', 'ucs2_ascii'];

// Module Exports
module.exports =
{
    init: function() {
        report.emit('screen', ink.hhtag  + modconf['TITLE']+' => '.highlight + 'initialized methods'); // Announce initialized module methods
    },
    list: function() {

    },
    lparams: function()	{
        var response = "";
        response += '------------------------------------------'.hline+'\n';
        response += modconf['TITLE'].highlight+'\n';
        response += '------------------------------------------'.hline+'\n';
        for (var param in params)
            response += param.info + ' => '.highlight + params[param].pvalue+'\n';

        response += '------------------------------------------'.hline+'\n';
        response += 'Supported Types'.norm+'\n';
        response += '-----------------'.hline+'\n';
        for (var type in types) {
            response += types[type].pvalue;
        }
        report.emit('screen', response);
    },
    set: function(key, value) {
        key = key.toUpperCase();
        params[key] = value;
        report.emit('screen', ink.hhtag  +modconf['TITLE']+' => '.highlight + 'param set' + ' => '.highlight + key + ' => '.highlight + value);	// Set param value
    },
    run: function()	{
        report.emit('screen', ink.hhtag  +modconf['TITLE']+' => '.highlight + 'run method => hash type ' + params['TYPE']);   // Launch run method session

        if (params['TYPE'] == 'base64') {
            report.emit('screen', new Buffer(params['STRING']).toString('base64') );
        } else if (params['TYPE'] == 'utf8') {
            report.emit('screen', new Buffer(params['STRING']).toString('utf8') );
        } else if (params['TYPE'] == 'ucs2') {
            report.emit('screen', new Buffer(params['STRING']).toString('ucs2') );
        } else if (params['TYPE'] == 'hex') {
            report.emit('screen', new Buffer(params['STRING']).toString('hex') );
        } else if (params['TYPE'] == 'base64_ascii') {
            report.emit('screen', new Buffer(params['STRING'], 'base64').toString('ascii') );
        } else if (params['TYPE'] == 'hex_ascii') {
            report.emit('screen', new Buffer(params['STRING'], 'hex').toString('ascii') );
        } else if (params['TYPE'] == 'utf8_ascii') {
            report.emit('screen', new Buffer(params['STRING'], 'utf8').toString('ascii') );
        } else if (params['TYPE'] == 'ucs2_ascii') {
            report.emit('screen', new Buffer(params['STRING'], 'ucs2').toString('ascii') );
        } else {
            report.emit('screen', 'That type of hash method is not recognized.'.error );
        }

        prompt.emit('prompt');
    },
    done: function() {
        report.emit('screen', ink.hhtag  +modconf['TITLE']+' => '.highlight + 'de-initialized methods' );	// Done using module
    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};
