# HACK.JS README


## ABOUT

Hack.JS aims to provide a dynamic Node.JS driven Penetration 
Testing Framework that can be extended by the user. 

HACK.JS supports dynamic loading of modules and new commands.


## INSTALLATION

git clone https://Cyb3rnetic@bitbucket.org/Cyb3rnetic/hjsframework.git

cd hjsframework

npm install


## COMMANDS

[COMMAND]	[DESCRIPTION]

clear	 	- Clear the screen.

cmdtest	 	- Just a test command.

exit	 	- Exit the application.

help	 	- Display this help information.

list	 	- List all modules.

lparams	 	- List a modules parameter options.

pull	 	- Perform a GIT Pull request to update the framework

run	 		- Launch the module's run method session.

set	 		- Set a modules parameters. set <key> <value>

sh	 		- Execute a shell command. sh <command>

top	 		- Display Linux processes.

use	 		- Initialize a module to use. use <module>

version	 	- Display current installed version.


## MODULES

[MODULE]	        [DESCRIPTION]

crypto/crypto	 	- This crypto module supplies access to algorithms supported by the version of OpenSSL on the platform.

fuzzer/example		- Example module that can be used as a template when starting new mods. 

hash/buffer	   		- Access to NodeJS Buffer for performing basic hashing commonly used when handling buffers.

intel/exploitdb		- Perform a search against exploit-db.com

intel/radars	  	- United States radar, satillite and map directory

intel/threats	 	- United States cyber threat intelligence directory

intel/traffic	 	- United States traffic camera directory

payload/paygen		- Create a Payload using Metasploit's MSFVenom

service/honeypot	- A basic honeypot with HTTP, TELNET, and FILE monitoring. See: ./lib/html/honeypot

service/http	  	- Basic HTTP service used for hosting browser exploits. See: ./lib/html/exploits

sniffer/backdoors	- Sniff out hidden backdoors on a remote website.

sniffer/dirdisco	- Sniff out directories on a web server.

sniffer/gdork	 	- Perform Google Dorks manually from inside the freamework and export to JSON/CSV

sniffer/infogat	    - Sniff out files left behind by installations, backups, and developers for loot.

sniffer/irc			- The IRC Sniffer will connect to a list of IRC servers and search for keywords in channel names, topics, nicknames etc..

sniffer/plecost		- Wordpress finger printer tool (with threads support) 0.2.2-9-beta. Install to ./vendors/

sniffer/xst	   		- Module used to detect Cross-Site Tracing (XST) vulnerabilities.
