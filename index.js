#!/home/<user>/.nvm/versions/node/v7.7.3/bin/node

/**
 * HACK.JS FRAMEWORK
 *
 * An open source Node.JS driven penetration testing and analysis framework
 */

/**
 * @type {exports|module.exports}
 *
 * Requirements
 */
var proc        = require('./lib/processor');               // Command and HJS prompt processor
var report      = require('./lib/report').reportEmitter;    // Reporting to Screen or Log File

/**
 * Start HJS Shell
 */
proc.start();

