/**
 * Title: version.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Display name and version from package.json
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tDisplay current installed version."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var fs = require('fs');

// Load package.json to get version
var PACK_DATA = JSON.parse(fs.readFileSync("./package.json", "utf8"));

var prompt = require('../processor').promptEmitter;
var report = require('../report').reportEmitter;

prompt.on('version', function() {
    report.emit('screen', "\n"+PACK_DATA.name+" - "+PACK_DATA.version+"\n"+PACK_DATA.description+"\n\n");
    prompt.emit('prompt');
});