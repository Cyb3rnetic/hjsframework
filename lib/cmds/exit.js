/**
 * Title: exit.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Exit application process
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tExit the application."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;

prompt.on('exit', function() {
    process.exit();
});
