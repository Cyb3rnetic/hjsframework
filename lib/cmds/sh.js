/**
 * Title: sh.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Run Linux command and return output
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tExecute a shell command. sh <command>"
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;
var report = require('../report').reportEmitter;

var exec = require('child_process').exec;

/**
 * prompt.on('sh', callback)
 * Run a Linux CLI command
 */
prompt.on('sh', function(data) {
    var fin_cmd = "";
    for(var i = 1; i<data.length; i++) {
        fin_cmd = fin_cmd + data[i] + " ";
    }
    //report.emit('screen', fin_cmd.trim());
    exec(fin_cmd.trim(), puts);// run linux bash command
});

function puts(error, stdout, stderr) { report.emit('screen', "\n"+stdout+"\n"); prompt.emit('prompt'); }