/**
 * Title: set.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Set active module's parameters
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tSet a modules parameters. set <key> <value>"
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};
/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;
var report = require('../report').reportEmitter;

var mh = require('../modhandler');

/**
 * prompt.on('set', callback)
 * Set active module's parameters
 */
prompt.on('set', function(c_file, data) {
    var fin_value = "";
    for(var i = 2; i<data.length; i++) {
        fin_value = fin_value + data[i] + " ";
    }

    if (c_file)
        mh.set(data[1], fin_value.trim()); // set parameter

    prompt.emit('prompt');
});