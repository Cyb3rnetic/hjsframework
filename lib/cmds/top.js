/**
 * Title: top.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Display Linux processes
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tDisplay Linux processes"
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../processor').CONFIG_DATA;

const exec      = require('child_process').exec;

var prompt      = require('../processor').promptEmitter;

var clear       = require('clear');
var chalk       = require('chalk');
var figlet      = require('figlet');

/**
 * clear
 * Clears the screen
 */
prompt.on('top', function() {
    exec('xterm -hold -bg black -fg green -geometry 90x30+0+0 -T "Top - Process Monitor" -e "top"', (e,stdout,stderr) => {
        if (e instanceof Error) {
         console.error(e);
         throw e; 
        }
        if (stderr) console.log('stderr ', stderr);
        if (stdout) console.log('stdout ', stdout);
      });
    prompt.emit('prompt');
});