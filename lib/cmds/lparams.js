/**
 * Title: lparams.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: List active module's parameters
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tList a modules parameter options."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;
var report = require('../report').reportEmitter;

var mh = require('../modhandler');

/**
 * prompt.on('lparams', callback)
 * List active module's parameters
 */
prompt.on('lparams', function(c_file) {
    if (c_file) {
        mh.lparams();
    }

    prompt.emit('prompt');
});