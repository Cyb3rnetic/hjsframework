/**
 * Title: list.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: List available modules
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tList all modules."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../processor').CONFIG_DATA;
var events      = require('events');
var prompt      = require('../processor').promptEmitter;
var report      = require('../report').reportEmitter;
var ink         = require('../colors');
var fs          = require('fs');
var mh          = require('../modhandler');
var reuse       = require('../reuseables');

/**
 * prompt.on('list', callback)
 * List available modules
 */
prompt.on('list', function() {
    modfiles = reuse.getFiles('modules');
    report.emit('screen', '\n\n'+CONFIG_DATA.CONFIGS.CORE.NAME.highlight+' Loaded Modules'.info+'\n-----------------------------\n'.norm);
    report.emit('screen', '\n[MODULE]\t        [DESCRIPTION]\n'.info);
    for (modfile in modfiles) {
        var modfi = '../' + modfiles[modfile];
        mh.list(modfi);
        var descri = mh.describe()
        report.emit('screen', modfiles[modfile].replace('modules/', '').replace('.js', '').highlight+'\t'+descri.info);
    }
    report.emit('screen', '\n-----------------------------\n\n'.norm);
    if (mh.old_c_file != undefined) mh.use(mh.old_c_file); // Need to reload the module that was in use after a list
    prompt.emit('prompt');
});

