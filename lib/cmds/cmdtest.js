/**
 * Title: cmdtest.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Simple command test
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tJust a test command."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;
//var report = require('../report').reportEmitter;

/**
 * prompt.on('cmdtest', callback)
 * Display a message to represent a working command
 */
prompt.on('cmdtest', function() {
    console.log('This is an imported command test');
    prompt.emit('prompt');
});
