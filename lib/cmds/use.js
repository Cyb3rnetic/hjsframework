/**
 * Title: use.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Use module in the modules directory
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tInitialize a module to use. use <module>"
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;
var report = require('../report').reportEmitter;

var mh = require('../modhandler');

/**
 * prompt.on('use', callback)
 * Use a module in the module directory
 */
prompt.on('use', function(modfile) {
    try {
        mh.c_file = '../modules/'+modfile+'.js';
        mh.old_c_file = '../modules/'+modfile+'.js';
        mh.use(mh.c_file);
        prompt.emit('prompt');
    } catch (e) {
        try { mh.done(); } catch (err) { } // run modules run method
        mh.c_file = undefined;
        mh.old_c_file = undefined;
        report.emit('screen', 'Error loading module\n\n'.error);
        prompt.emit('prompt');
    }
});