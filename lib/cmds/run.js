/**
 * Title: run.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Run active modules execution process against set module parameters
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tLaunch the module's run method session."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var prompt = require('../processor').promptEmitter;
var report = require('../report').reportEmitter;

var mh = require('../modhandler');

/**
 * prompt.on('run', callback)
 * Run active module's execution process against set parameters
 */
prompt.on('run', function(c_file) {
    if (c_file) {
        mh.run();
    } else {
    	prompt.emit('prompt');
    }
});