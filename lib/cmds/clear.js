/**
 * Title: clear.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Clear the screen displaying the ascii banner
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

// Command config
var modconf = {
    "DESCRIPTION": " \tClear the screen."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../processor').CONFIG_DATA;

var prompt      = require('../processor').promptEmitter;

var clear       = require('clear');
var chalk       = require('chalk');
var figlet      = require('figlet');

/**
 * clear
 * Clears the screen
 */
prompt.on('clear', function() {
    clear();
    console.log(
        chalk.yellow(
            figlet.textSync(CONFIG_DATA.CONFIGS.CORE.NAME + ' FRAMEWORK', { horizontalLayout: 'full' })
        )
    );
    prompt.emit('prompt');
});