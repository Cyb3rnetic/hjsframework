/**
 * Title: help.js
 * Type: Command
 * Author: Hack.JS Framework
 * Description: Display available help
 * Created Date: 2017-03-20
 * Last Modified: 2017-03-20
 */

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var CONFIG_DATA = require('../processor').CONFIG_DATA;
var prompt      = require('../processor').promptEmitter;
var report      = require('../report').reportEmitter;
var ink         = require('../colors');
var fs          = require('fs');
var mh          = require('../modhandler');
var reuse       = require('../reuseables');

// Command config
var modconf = {
    "DESCRIPTION": " \tDisplay this help information."
}

module.exports =
{
    list: function() {

    },
    describe: function() {
        return modconf['DESCRIPTION'];
    }
};

/**
 * prompt.on('help', callback)
 * Display available command help
 */
prompt.on('help', function() {
    cmdfiles = reuse.getFiles('./lib/cmds');
    report.emit('screen', '\n\n'+CONFIG_DATA.CONFIGS.CORE.NAME.highlight+' Loaded Commands'.info+'\n-----------------------------\n'.norm);
    report.emit('screen', '\n[COMMAND]\t[DESCRIPTION]\n'.info);
    for (cmdfile in cmdfiles) {
        var cmd = '../' + cmdfiles[cmdfile];
        mh.list(cmd);
        var descri = mh.describe()
        report.emit('screen', cmdfiles[cmdfile].replace('./lib/cmds/', '').replace('.js', '').highlight+'\t'+descri.info);
    }
    report.emit('screen', '\n-----------------------------\n\n'.norm);
    if (mh.old_c_file != undefined) mh.use(mh.old_c_file); // Need to reload the module that was in use after help
    prompt.emit('prompt');
});