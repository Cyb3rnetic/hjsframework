/**
 * processor.js
 * Main application and command prompt processor
 */
/**
 * @type {Chalk|*|exports|module.exports}
 * Requirements
 */
const events          = require('events');

var PACK_DATA         = require('../package');
var CONFIG_DATA       = require('../config');

exports.PACK_DATA     = PACK_DATA;
exports.CONFIG_DATA   = CONFIG_DATA;

var chalk             = require('chalk');
var clear             = require('clear');
var CLI               = require('clui');
var figlet            = require('figlet');
var inquirer          = require('inquirer');
var Preferences       = require('preferences');
var Spinner           = CLI.Spinner;
var _                 = require('lodash');
var git               = require('simple-git')();
var touch             = require('touch');
var fs                = require('fs');
var util              = require('util');
var exec              = require('child_process').execSync;
var prefs             = new Preferences('jhackframework');
var ink               = require('./colors');
var report            = require('./report').reportEmitter;
var promptEmitter     = new events.EventEmitter();
exports.promptEmitter = promptEmitter;

var promptSwitch      = require('./switch').promptSwitch;
var banners           = require('./banners');
var cmds              = require('./cmds');
cmds.load(process.cwd()+'/lib/cmds/');

var robot = require("robotjs");
var cmd_history = [];
var ch_count = 0;

/**
 * start()
 * Start the command processing CLI
 */
exports.start = function() {
  console.log(
      chalk.yellow(
          figlet.textSync(CONFIG_DATA.CONFIGS.CORE.NAME + ' FRAMEWORK', { horizontalLayout: 'full' })
      )
  );

  hjs(function(){
    procCmd(arguments);
  });
}

/**
 * hjs(callback)
 * @param callback
 * @description Print the prompt to th user
 */
function hjs(callback) {
  var questions = [
    {
      name: 'cmd',
      type: 'input',
      message: 'hjs~:',
      validate: function( value ) {
        if (value.length) {
          return true;
        } else {
          return '';
        }
      }
    }
  ];

  inquirer.prompt(questions).then(callback);
}

/**
 * exit()
 * Exit the application
 */
function exit() {
    process.exit();
};

/**
 * prompt(arguments)
 * @param arguments
 * Call the CLI prompt with procCmd(arguments) callback.
 */
function prompt(arguments) {
  hjs(function(){
    procCmd(arguments);
  });
};

/**
 * procCmd(arguments)
 * @param arguments
 * Pre-process the arguments returned from user via prompt
 */
function procCmd(arguments) {
  cmd_history.push(arguments[0].cmd);
  promptSwitch(arguments);
};

promptEmitter.on('prompt', function(){
  prompt();
});

/**
 * Command history implementation
 */
stdin = process.stdin;
stdin.on('keypress', function (ch, key) {
  if (key.name == 'up') {
    var longest = cmd_history.sort(function (a, b) { return b.length - a.length; })[0].length;
    for (var i = 0; i < longest; i++) {
      robot.keyTap('backspace');
    }
    if (ch_count >= cmd_history.length) ch_count = 0;
    robot.typeString(cmd_history[ch_count]);
    ch_count++;
  }

  if (key.name == 'down') {
    var longest = cmd_history.sort(function (a, b) { return b.length - a.length; })[0].length;
    for (var i = 0; i < longest; i++) {
      robot.keyTap('backspace');
    }
    ch_count--;
    if ((ch_count < (cmd_history.length - cmd_history.length))) ch_count = 0;
    robot.typeString(cmd_history[ch_count]);
  }
});

/**
 * Verbose load
 */
if(CONFIG_DATA.CONFIGS.CORE.VERBOSE_START) {
  report.emit('screen', ink.itag + 'Commands Loaded ........................  '+ '['.input.bold+ banners.getLoadedCmds().toString().info.bold+'] '.input.bold);
  report.emit('screen', ink.itag + 'Modules Loaded .........................  '+ '['.input.bold+ banners.getLoadedModules().toString().info.bold+'] '.input.bold);
  report.emit('screen', ink.itag + 'Processor loaded   .....................  ' + ink.oktag);
  report.emit('screen', ink.itag + 'Welcome to '+CONFIG_DATA.CONFIGS.CORE.NAME+'  ....................  ' + ink.oktag);
}
