/**
 * banners.js
 * Banners and stats
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var fs              = require('fs')
var ansi            = ['./lib/ansi/hx-doors.ans']; //, './lib/ansi/file_id.ans']
var report          = require('./report').reportEmitter;
var ink             = require('./colors');
var reuse           = require('./reuseables');
var loaded_modules  = 0;
var loaded_cmds     = 0;

/**
 * banners()
 * @type {banners}
 * ANSI banners
 */
exports.banners = banners;
function banners() {
  fs.readFile(ansi[randomInt(0,1)], 'utf8', function (err,data) {
    if (err) {
      return report.emit('screen', err);
    }
    report.emit('screen', "\n\n");
    report.emit('screen', data);
    report.emit('screen', "\n\n");
    //info();
  });
}

/**
 * info()
 * @type {info}
 * Emit number of loaded modules message
 */
exports.info = info;
function info() {
    modfiles = reuse.getFiles('modules');
    var i=0;
    for (modfile in modfiles) {
        i++;
    }
    report.emit('screen', ink.itag+'Modules Loaded .........................  '+ '['.input.bold+ i.toString().info.bold+'] '.input.bold);

    loaded_modules = i;
}

/**
 * randomInt(low, high)
 * @param low
 * @param high
 * @returns {number}
 * Returns a random INT between low and high
 */
function randomInt (low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

/**
 * getLoadedModules
 * @returns {number}
 * Return number of loaded modules
 */
function getLoadedModules() {
    modfiles = reuse.getFiles('modules');
    var i=0;
    for (modfile in modfiles) {
        i++;
    }

    loaded_modules = i;
    return loaded_modules;
}
exports.getLoadedModules = getLoadedModules;

/**
 * getLoadedCmds
 * @returns {number}
 * Return number of loaded commands
 */
function getLoadedCmds() {
    cmdfile = reuse.getFiles('./lib/cmds');
    var i=0;
    for (cmdfile in cmdfile) {
        i++;
    }

    loaded_cmds = i;
    return loaded_cmds;
}
exports.getLoadedCmds = getLoadedCmds;
