var fs = require('fs');

/**
 * getFiles(dir, files_)
 * @param dir
 * @param files_
 * @returns {*|Array}
 * Return a list of files
 */
function getFiles (dir, files_){
    files_ = files_ || [];
    var files = fs.readdirSync(dir);
    for (var i in files){
        var name = dir + '/' + files[i];
        if (fs.statSync(name).isDirectory()){
            getFiles(name, files_);
        } else {
            files_.push(name);
        }
    }
    return files_;
}

exports.getFiles = getFiles;

