/**
 * colors.js
 *
 * Console Logging Colors Layer
 */

/**
 * @type {*|exports|module.exports}
 * Requirements
 */
var colors = require('colors');

// Exports
exports.colors = colors;

// Color Set Theme
colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  hline: 'blue',
  error: 'red',
  pvalue: 'red',
  highlight: 'yellow',
  norm: 'white'
});

exports.itag = '['.input.bold+'+'.info.bold +'] '.input.bold;
exports.oktag = '['.input.bold+'ok'.info.bold +'] '.input.bold;
exports.etag = '['.input.bold+'-'.error.bold+'] '.input.bold;
exports.wtag = '['.input.bold+'!'.warn.bold +'] '.input.bold;
exports.dtag = '['.help.bold +'*'.warn.bold +'] '.help.bold;

// Module tags
exports.cotag = '['.input.bold+'CRYP'.info.bold +'] '.input.bold;
exports.frtag = '['.input.bold+'FUZZ'.info.bold +'] '.input.bold;
exports.hhtag = '['.input.bold+'HASH'.info.bold +'] '.input.bold;
exports.pdtag = '['.input.bold+'PAYL'.info.bold +'] '.input.bold;
exports.setag = '['.input.bold+'SERV'.info.bold +'] '.input.bold;
exports.srtag = '['.input.bold+'SNIFF'.info.bold +'] '.input.bold;
exports.intag = '['.input.bold+'INTEL'.info.bold +'] '.input.bold;

