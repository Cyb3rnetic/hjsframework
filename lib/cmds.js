/**
 * cmds.js
 *
 * Load all commands in the ./cmds/ directory
 */

/**
 * @type {exports|module.exports}
 * Requirements
 */
var fs = require('fs');

/**
 * @type {load}
 */
exports.load = load;

/**
 * load(dir)
 * @param dir
 * Load the commands in dir
 */
function load(dir) {
    var files = fs.readdirSync(dir);

    for(var i=0, cnt=1; i < files.length; i++,cnt++) {
        var path = dir + files[i];
        if(fs.statSync(path).isFile()) {
            var cmd = require(path);
        }
    }
}