/**
 * modhandler.js
 * Module handler for processing mod commands.
 */

/**
 * @type {use}
 * Requirements
 */
exports.use = use;
exports.lparams = lparams;
exports.set = set;
exports.run = run;
exports.done = done;
exports.describe = describe;
exports.list = list;
exports.c_file = c_file;
exports.old_c_file = old_c_file;

/**
 * @type {undefined}
 */
var handler = undefined;
var c_file = undefined;
var old_c_file = undefined;

/**
 * use(file)
 * @param file
 * Use a module
 */
function use(file) {
    c_file = file;
    handler = require(c_file);
    var method = 'init';
    //if(typeof(controller[method])=='Function')
    handler[method]();
}

/**
 * list(file)
 * @param file
 * List modules in ../modules
 */
function list(file) {
    c_file = file;
    handler = require(c_file);
    var method = 'list';
    //if(typeof(controller[method])=='Function')
    handler[method]();
}

/**
 * lparams()
 * List module parameters
 */
function lparams() {
    handler = require(c_file);
    var method = 'lparams';
    //if(typeof(controller[method])=='Function')
    handler[method]();
}

/**
 * set(key, value)
 * @param key
 * @param value
 * Set a modules parameters by key and value
 */
function set(key, value) {
    handler = require(c_file);
    var method = 'set';
    //if(typeof(controller[method])=='Function')
    handler[method](key, value);
}

/**
 * run()
 * Run the module with set parameters
 */
function run() {
    handler = require(c_file);
    var method = 'run';
    //if(typeof(controller[method])=='Function')
    handler[method]();
}

/**
 * done()
 * Unload the module in use
 */
function done() {
    handler = require(c_file);
    var method = 'done';
    //if(typeof(controller[method])=='Function')
    handler[method]();
    c_file = undefined;
    old_c_file = undefined;
    handler = undefined;
}

/**
 * describe()
 * @returns {*}
 * Return the discription of the module
 */
function describe() {
    handler = require(c_file);
    var method = 'describe';
    var response = handler[method]();
    c_file = undefined;
    handler = undefined;
    return response;
}
