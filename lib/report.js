/**
 * reportEmitter
 * Report to screen, file etc..
 */

/**
 * @type {EventEmitter|exports|module.exports}
 * Requirements
 */
var CONFIG_DATA         = require('./processor').CONFIG_DATA;
var events              = require('events');
var ink                 = require('./colors');
var fs                  = require('fs');
var reportEmitter       = new events.EventEmitter();
exports.reportEmitter   = reportEmitter;

var doLog               = true;

/**
 * reportEmitter.on(type, msg)
 * Report to log file
 */
reportEmitter.on('log', function(msg) {
    fs.appendFile(CONFIG_DATA.CONFIGS.CORE.FILELOG, msg, function (err) {
        if (err) throw err;
        //console.log('The "data to append" was appended to file!');
    });
});


/**
 * reportEmitter.on(type, msg)
 * Report to screen
 */
reportEmitter.on('screen', function(msg) {
    console.log(msg);
    if (CONFIG_DATA.CONFIGS.CORE.LOGGING === true) {
        fileLog(msg.replace(/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/g, ''));
    }
});

//reportEmitter.on('dashboard', function(type, msg) {
//    io.emit(type, msg.replace(/\x1B\[([0-9]{1,2}(;[0-9]{1,2})?)?[m|K]/g, ''));
//});

/**
 * fileLog(msg)
 * @param msg
 */
function fileLog(msg) {
    fs.appendFile(CONFIG_DATA.CONFIGS.CORE.FILELOG, msg, function (err) {
        if (err) throw err;
        //console.log('The "data to append" was appended to file!');
    });
}

/**
 * Verbose load
 */
if(CONFIG_DATA.CONFIGS.CORE.VERBOSE_START) {
    reportEmitter.emit('screen', ink.itag + 'Reporting Events imported  .............  ' + ink.oktag);
    reportEmitter.emit('screen', ink.itag + 'Reporting Emitter activated  ...........  ' + ink.oktag);
}
