

var mh      = require('./modhandler');               // Module Handler
var c_file  = undefined;                             // c_file : the module in use or being loaded
var events  = require('events');                     // Events manager
var prompt  = require('./processor').promptEmitter;  // Prompt Emitter
var reuse   = require('./reuseables')                // Reuseables

exports.promptSwitch = promptSwitch;

function promptSwitch(data) {
        try {
            hjscmd = data[0].cmd;
            var args = data[0].cmd.split(' ');
        } catch (err) { hjscmd = ['exit']; } // ctrl+c fix

        switch (args[0]) {
            // USE
            case 'use':
                if (args[1] != undefined) {
                    c_file = '../modules/'+args[1]+'.js';
                    prompt.emit('use', args[1]);
                } else {
                    prompt.emit('prompt');
                }
                break;
            // LPARAMS
            case 'lparams':
                prompt.emit('lparams', c_file);
                break;
            // SET
            case 'set':
                prompt.emit('set', c_file, args);
                break;
            // RUN
            case 'run':
                prompt.emit('run', c_file);
                break;
            // DONE
            case 'done':
                if (mh != undefined) {
                    try { mh.done(); } catch (err) {  } // run modules run method
                    c_file = undefined;
                    mh.c_file = undefined;
                    mh.old_c_file = undefined;
                }
                prompt.emit('prompt');
                break;
            // SH
            case 'sh':
                prompt.emit('sh', args);
                break;
            default:
                var status = 0;
                var cmdfiles = reuse.getFiles('./lib/cmds');
                for (cmdfile in cmdfiles) {
                    if (args[0] == cmdfiles[cmdfile].replace('./lib/cmds/','').replace('.js','')) {
                        status = 1;
                    }
                }

                if (status) {
                    prompt.emit(hjscmd);
                } else {
                    console.log("Command does not exist!");
                    prompt.emit('prompt');
                }

                break;
        }
}
